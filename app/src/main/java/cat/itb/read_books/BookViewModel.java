package cat.itb.read_books;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class BookViewModel extends ViewModel {
    static List<Book> llibres = new ArrayList<Book>();

    public List<Book> getLlibres() {
        return llibres;
    }

    String[] autors = {"Roger","Josep","Manel","Angel Guimerà","Stephen Meyer"};
    String[] titols = {"Gran historia","Petita historia","Mitjana Historia","Quina bona historia","Mare meva..."};
    String[] status = {"Read","Read","Want to read","Reading","Reading"};
    int[] valors = {4,2,0,0,0};

    public BookViewModel() {
        Book b;
        for (int i = 0; i < 5; i++) {
            b = new Book(titols[i],autors[i],status[i], valors[i]);
            llibres.add(b);
        }
    }
}
