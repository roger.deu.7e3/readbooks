package cat.itb.read_books;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class book_fragment extends Fragment {

    private Spinner spinner;
    private EditText autor, titol;
    private RatingBar ratingBar;
    private Book book;
    private Button buttonAdd;
    private int opcio = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        book = new Book();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_book_fragment,container,false);

        spinner = v.findViewById(R.id.spinner);
        autor = v.findViewById(R.id.autor);
        titol = v.findViewById(R.id.titol);
        ratingBar = v.findViewById(R.id.ratingBar);
        buttonAdd = v.findViewById(R.id.buttonAdd);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),R.array.status,R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        if (getArguments() != null) {
            book = getArguments().getParcelable("book");
        }
        if (book!=null){
            autor.setText(book.getAuthor());
            titol.setText(book.getTitle());
            ratingBar.setRating(book.getRate());
            buttonAdd.setVisibility(View.INVISIBLE);
        } else {
            book = new Book();
            opcio = 1;
        }

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (titol.getText().toString().isEmpty() || autor.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), "Error: faltan campos obligatorios", Toast.LENGTH_SHORT).show();
                } else {
                    Book llibre = new Book(titol.getText().toString(),autor.getText().toString(),spinner.getSelectedItem().toString(), (int) ratingBar.getRating());
                    BookViewModel.llibres.add(llibre);
                    Navigation.findNavController(v).navigate(R.id.action_book_fragment_to_bookList);
                }

            }
        });

        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if ((titol.getText().toString().isEmpty() || autor.getText().toString().isEmpty()) && opcio == 0) {
                    Toast.makeText(getContext(), "Error: faltan campos obligatorios", Toast.LENGTH_SHORT).show();
                } else {
                    remove();
                }
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        autor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                book.setAuthor(autor.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        titol.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                book.setTitle(titol.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                book.setRate((int) rating);
            }
        });



        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String status =parent.getItemAtPosition(position).toString();
                book.setStatus(status);
                if (status.equalsIgnoreCase("read")) {
                    status = "read";
                    ratingBar.setEnabled(true);
                } else {
                    ratingBar.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                book.setStatus("");
            }
        });
    }
}