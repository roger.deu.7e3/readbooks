package cat.itb.read_books;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {
    List<Book> books;

    public BookAdapter(List<Book> books) {
        this.books = books;
    }


    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_list_item,parent,false);
        return new BookViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        holder.bindData(books.get(position));
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    class BookViewHolder extends RecyclerView.ViewHolder{
        TextView titol, autor, status;
        RatingBar ratingBar;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);

            titol = itemView.findViewById(R.id.titol);
            autor = itemView.findViewById(R.id.autor);
            status = itemView.findViewById(R.id.status);
            ratingBar = itemView.findViewById(R.id.rating);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavDirections listToFragmentDirections = BookListDirections.actionBookListToBookFragment(books.get(getAdapterPosition()));
                    Navigation.findNavController(v).navigate(listToFragmentDirections);
                }
            });
        }

        public void bindData(Book book) {
            titol.setText(book.getTitle());
            autor.setText(book.getAuthor());
            status.setText(book.getStatus());
            ratingBar.setRating(book.getRate());
        }

    }
}
